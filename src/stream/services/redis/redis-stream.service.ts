import { RedisStreamProducer } from "./redis-stream.producer";
import { RedisStreamConsumer } from "./redis-stream.consumer";
import { StreamConsumerHandlerInterface, StreamServiceInterface } from "../../interfaces";
import { LoggerService } from "@nestjs/common";
import { RedisStream } from "./redis-stream";
import { ConsumerHandlersHandler } from "../consumer-handlers-handler";

export class RedisStreamService implements StreamServiceInterface {

  private producers: RedisStreamProducer[];
  private consumers: RedisStreamConsumer[];

  constructor(private readonly logger: LoggerService,
              private readonly streams: RedisStream[]) {
    this._initProducers();
    this._initConsumers();
  }

  private _initProducers() {
    this.producers = [];
    for (const stream of this.streams) {
      this.producers.push(new RedisStreamProducer(this.logger, stream));
    }
  }

  private _initConsumers() {
    this.consumers = [];
    for (let i = 0; i < this.streams.length; i++) {

      const stream = this.streams[i];

      stream.initializeConsumerGroup(i + 1);
      stream.initializeConsumerName(i + 1);

      this.consumers.push(new RedisStreamConsumer(this.logger, stream));
    }
  }

  public add(message: any): Promise<any> {

    const promises = [];

    for (const producer of this.producers) {
      promises.push(producer.add(message));
    }

    return Promise.all(promises);
  }

  public consume(onUpdate: (message: any) => void): Promise<StreamConsumerHandlerInterface> {

    const promises: Promise<StreamConsumerHandlerInterface>[] = [];

    for (const consumer of this.consumers) {
      promises.push(consumer.consume(onUpdate));
    }

    return Promise.all(promises)
      .then((handlers: StreamConsumerHandlerInterface[]) => {
        return new ConsumerHandlersHandler(handlers);
      });
  }

}