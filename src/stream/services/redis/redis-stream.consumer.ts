import { RedisStream } from "./redis-stream";
import { StreamConsumerInterface } from "../../interfaces/stream-consumer.interface";
import { LoggerService } from "@nestjs/common";
import { StreamConsumerHandlerInterface } from "../../interfaces/stream-consumer-handler.interface";
import { IntervalConsumerHandler } from "../interval-consumer-handler";

export class RedisStreamConsumer implements StreamConsumerInterface {

  private static readonly READ_RATE_IN_MILLISECONDS = 10;

  private _ready: boolean;

  constructor(
    private readonly logger: LoggerService,
    private readonly stream: RedisStream) {
    this._ready = true;
  }

  public consume(onEvent: (message: any) => void): Promise<StreamConsumerHandlerInterface> {

    return this.createStream()
      .then(() => {
        return this.createConsumerGroup();
      })
      .then(() => {
        return this.consumeGroup(onEvent);
      });
  }

  private createStream(): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        this.stream.getService().getClient().xadd(
          this.stream.getStreamId(),
          "*",
          this.stream.getObjectKey(),
          "", (error) => {
            if (!error) {
              resolve();
            } else {
              reject(error);
            }
          });
      } catch (e) {
        reject("Error creating stream: " + e);
      }
    });
  }

  private createConsumerGroup(): Promise<any> {

    return new Promise((resolve, reject) => {
      try {
        // noinspection TypeScriptUnresolvedFunction
        this.stream.getService().getClient().xgroup(
          "CREATE",
          this.stream.getStreamId(),
          this.stream.getConsumerGroup(),
          "$",
          (error) => {
            if (!error || error.code === "BUSYGROUP") {
              resolve();
            } else {
              this.logger.error("error" + error);
              reject(error);
            }
          });
      } catch (e) {
        reject("Error creating consumer group: " + e);
      }
    });
  }

  private consumeGroup(onEvent: (message) => void): Promise<StreamConsumerHandlerInterface> {
    return new Promise<StreamConsumerHandlerInterface>((resolve, reject) => {
      const intervalId = setInterval(() => {
        if (this._ready) {
          this._ready = false;
          this.readGroup(reject, onEvent);
        }
      }, RedisStreamConsumer.READ_RATE_IN_MILLISECONDS);
      resolve(new IntervalConsumerHandler(intervalId));
    });
  }

  private readGroup(reject, onEvent: (message: any) => void) {

    try {
      // noinspection TypeScriptUnresolvedFunction
      this.stream.getService().getClient().xreadgroup(
        "GROUP",
        this.stream.getConsumerGroup(),
        this.stream.getConsumerName(),
        "NOACK",
        "STREAMS",
        this.stream.getStreamId(),
        ">",
        (error, rawMessage) => {
          if (!error) {
            if (rawMessage !== "null" && rawMessage !== null) {
              onEvent(rawMessage);
            }
          } else {
            this.logger.error("Error" + error);
            reject(error);
          }
          this._ready = true;
        });
    } catch (e) {
      this.logger.error("Error reading stream: " + e);
    }
  }

}