import { RedisService } from "../../../services";

export class RedisStream {

  constructor(
    private readonly client: RedisService,
    private readonly streamId: string,
    private readonly messageId: string,
    private readonly messageVersion: string,
    private consumerGroup?: string,
    private consumerName?: string) {
  }

  public getStreamId(): string {
    return this.streamId;
  }

  public getObjectKey(): string {
    return this.messageId + "-" + this.messageVersion;
  }

  public getService(): RedisService {
    return this.client;
  }

  public getConsumerGroup(): string {
    return this.consumerGroup;
  }

  public getConsumerName(): string {
    return this.consumerName;
  }

  public initializeConsumerGroup(consumerGroupNumber: number) {
    if (!this.consumerGroup) {
      this.consumerGroup = this.messageId + "-consumer-group-" + consumerGroupNumber;
    }
  }

  public initializeConsumerName(consumerNumber: number) {
    if (!this.consumerName) {
      this.consumerName = this.messageId + "-consumer-" + consumerNumber;
    }
  }

}