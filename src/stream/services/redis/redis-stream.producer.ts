import { RedisStream } from "./redis-stream";
import { StreamProducerInterface } from "../../interfaces/stream-producer.interface";
import { LoggerService } from "@nestjs/common";

export class RedisStreamProducer implements StreamProducerInterface {

  constructor(private readonly logger: LoggerService, private readonly stream: RedisStream) {}

  public add(message: any): Promise<any> {
    return this.stream.getService().getClient().xadd(
      this.stream.getStreamId(),
      "*",
      this.stream.getObjectKey(),
      JSON.stringify(message), (err) => {
        if (err) {
          this.logger.error(err);
        }
      });
  }
}