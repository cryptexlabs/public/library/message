import { StreamConsumerHandlerInterface } from "../interfaces";
import Timeout = NodeJS.Timeout;

export class IntervalConsumerHandler implements StreamConsumerHandlerInterface {

  constructor(private readonly intervalId: Timeout) {
  }

  public cancel() {
    clearInterval(this.intervalId);
  }
}