import { StreamConsumerHandlerInterface } from "../interfaces";

export class ConsumerHandlersHandler implements StreamConsumerHandlerInterface {

  constructor(private readonly handlers: StreamConsumerHandlerInterface[]) {
  }

  public cancel() {
    for (const handler of this.handlers) {
      handler.cancel();
    }
  }
}