export interface StreamProducerInterface {
  add(message: any): Promise<any>;
}