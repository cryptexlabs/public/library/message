import { StreamConsumerHandlerInterface } from "./stream-consumer-handler.interface";

export interface StreamConsumerInterface {
  consume(onUpdate: (message: any) => void): Promise<StreamConsumerHandlerInterface>;
}