import { ClientConfigInterface } from "../client-config.interface";
import { RedisClient } from "redis";
import { LoggerService } from "@nestjs/common";
import redis = require("redis");

export class RedisService {

  private _client: RedisClient;
  private _isDisconnected: boolean;
  private _restartCount: number;

  constructor(private readonly logger: LoggerService, private readonly config: ClientConfigInterface) {
    this._initializeRedis();
    this._monitorRedis();
  }

  public getClient(): any {
    return this._client;
  }

  private _monitorRedis() {
    setInterval(() => {
      if (this._isDisconnected) {
        this._reInitializeRedis();
      }
    }, 1000);
  }

  private _initializeRedis() {
    this._isDisconnected = false;
    this._client         = redis.createClient(
      this.config.port,
      this.config.host,
    );
    this._client.on("connect", () => {
      this.logger.log("Redis: Connected to endpoint: " + this.config.host + ":" + this.config.port);
    });
    this._client.on("error", (err) => {
      this.logger.error("Redis: Error encountered: " + err);
    });
    this._client.on("end", () => {
      this.logger.error("Redis: Connection closed");
      this._isDisconnected = true;
    });
  }

  private _reInitializeRedis() {
    if (this._restartCount <= 3) {
      this._initializeRedis();
      this._restartCount++;
    } else {
      this.logger.error("Redis: Max retry count reached for " + this.config.host + ":" + this.config.port);
      process.exit(1);
    }
  }

}