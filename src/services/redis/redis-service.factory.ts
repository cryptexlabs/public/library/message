import { RedisService } from "./redis.service";
import { LoggerService } from "@nestjs/common";

export class RedisServiceFactory {

  private static instance: RedisServiceFactory;

  private services: RedisService[];

  private constructor() {
    this.services = [];
  }

  private static getInstance(): RedisServiceFactory {
    if (!this.instance) {
      this.instance = new RedisServiceFactory();
    }

    return this.instance;
  }

  private _getService(logger: LoggerService, host: string, port: number, user?: string, password?: string): RedisService {
    const endpoint = host + ":" + port;
    if (!this.services[endpoint]) {
      this.services[endpoint] = new RedisService(logger, {
        host, port, user, password,
      });
    }

    return this.services[endpoint];
  }

  public static getRedisService(logger: LoggerService, host: string, port: number, user?: string, password?: string): RedisService {
    return this.getInstance()._getService(logger, host, port, user, password);
  }

}