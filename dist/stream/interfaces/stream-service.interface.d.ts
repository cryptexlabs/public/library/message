import { StreamConsumerInterface } from "./stream-consumer.interface";
import { StreamProducerInterface } from "./stream-producer.interface";
export interface StreamServiceInterface extends StreamConsumerInterface, StreamProducerInterface {
}
