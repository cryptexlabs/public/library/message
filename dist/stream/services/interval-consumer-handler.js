"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class IntervalConsumerHandler {
    constructor(intervalId) {
        this.intervalId = intervalId;
    }
    cancel() {
        clearInterval(this.intervalId);
    }
}
exports.IntervalConsumerHandler = IntervalConsumerHandler;
//# sourceMappingURL=interval-consumer-handler.js.map