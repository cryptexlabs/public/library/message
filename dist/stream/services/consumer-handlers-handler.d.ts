import { StreamConsumerHandlerInterface } from "../interfaces";
export declare class ConsumerHandlersHandler implements StreamConsumerHandlerInterface {
    private readonly handlers;
    constructor(handlers: StreamConsumerHandlerInterface[]);
    cancel(): void;
}
