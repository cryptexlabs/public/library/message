"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redis_stream_producer_1 = require("./redis-stream.producer");
const redis_stream_consumer_1 = require("./redis-stream.consumer");
const consumer_handlers_handler_1 = require("../consumer-handlers-handler");
class RedisStreamService {
    constructor(logger, streams) {
        this.logger = logger;
        this.streams = streams;
        this._initProducers();
        this._initConsumers();
    }
    _initProducers() {
        this.producers = [];
        for (const stream of this.streams) {
            this.producers.push(new redis_stream_producer_1.RedisStreamProducer(this.logger, stream));
        }
    }
    _initConsumers() {
        this.consumers = [];
        for (let i = 0; i < this.streams.length; i++) {
            const stream = this.streams[i];
            stream.initializeConsumerGroup(i + 1);
            stream.initializeConsumerName(i + 1);
            this.consumers.push(new redis_stream_consumer_1.RedisStreamConsumer(this.logger, stream));
        }
    }
    add(message) {
        const promises = [];
        for (const producer of this.producers) {
            promises.push(producer.add(message));
        }
        return Promise.all(promises);
    }
    consume(onUpdate) {
        const promises = [];
        for (const consumer of this.consumers) {
            promises.push(consumer.consume(onUpdate));
        }
        return Promise.all(promises)
            .then((handlers) => {
            return new consumer_handlers_handler_1.ConsumerHandlersHandler(handlers);
        });
    }
}
exports.RedisStreamService = RedisStreamService;
//# sourceMappingURL=redis-stream.service.js.map