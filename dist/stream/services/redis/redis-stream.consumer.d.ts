import { RedisStream } from "./redis-stream";
import { StreamConsumerInterface } from "../../interfaces/stream-consumer.interface";
import { LoggerService } from "@nestjs/common";
import { StreamConsumerHandlerInterface } from "../../interfaces/stream-consumer-handler.interface";
export declare class RedisStreamConsumer implements StreamConsumerInterface {
    private readonly logger;
    private readonly stream;
    private static readonly READ_RATE_IN_MILLISECONDS;
    private _ready;
    constructor(logger: LoggerService, stream: RedisStream);
    consume(onEvent: (message: any) => void): Promise<StreamConsumerHandlerInterface>;
    private createStream;
    private createConsumerGroup;
    private consumeGroup;
    private readGroup;
}
