import { StreamConsumerHandlerInterface, StreamServiceInterface } from "../../interfaces";
import { LoggerService } from "@nestjs/common";
import { RedisStream } from "./redis-stream";
export declare class RedisStreamService implements StreamServiceInterface {
    private readonly logger;
    private readonly streams;
    private producers;
    private consumers;
    constructor(logger: LoggerService, streams: RedisStream[]);
    private _initProducers;
    private _initConsumers;
    add(message: any): Promise<any>;
    consume(onUpdate: (message: any) => void): Promise<StreamConsumerHandlerInterface>;
}
