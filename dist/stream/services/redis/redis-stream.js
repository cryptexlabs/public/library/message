"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RedisStream {
    constructor(client, streamId, messageId, messageVersion, consumerGroup, consumerName) {
        this.client = client;
        this.streamId = streamId;
        this.messageId = messageId;
        this.messageVersion = messageVersion;
        this.consumerGroup = consumerGroup;
        this.consumerName = consumerName;
    }
    getStreamId() {
        return this.streamId;
    }
    getObjectKey() {
        return this.messageId + "-" + this.messageVersion;
    }
    getService() {
        return this.client;
    }
    getConsumerGroup() {
        return this.consumerGroup;
    }
    getConsumerName() {
        return this.consumerName;
    }
    initializeConsumerGroup(consumerGroupNumber) {
        if (!this.consumerGroup) {
            this.consumerGroup = this.messageId + "-consumer-group-" + consumerGroupNumber;
        }
    }
    initializeConsumerName(consumerNumber) {
        if (!this.consumerName) {
            this.consumerName = this.messageId + "-consumer-" + consumerNumber;
        }
    }
}
exports.RedisStream = RedisStream;
//# sourceMappingURL=redis-stream.js.map