import { RedisService } from "../../../services";
export declare class RedisStream {
    private readonly client;
    private readonly streamId;
    private readonly messageId;
    private readonly messageVersion;
    private consumerGroup?;
    private consumerName?;
    constructor(client: RedisService, streamId: string, messageId: string, messageVersion: string, consumerGroup?: string, consumerName?: string);
    getStreamId(): string;
    getObjectKey(): string;
    getService(): RedisService;
    getConsumerGroup(): string;
    getConsumerName(): string;
    initializeConsumerGroup(consumerGroupNumber: number): void;
    initializeConsumerName(consumerNumber: number): void;
}
