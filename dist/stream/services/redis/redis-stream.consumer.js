"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const interval_consumer_handler_1 = require("../interval-consumer-handler");
class RedisStreamConsumer {
    constructor(logger, stream) {
        this.logger = logger;
        this.stream = stream;
        this._ready = true;
    }
    consume(onEvent) {
        return this.createStream()
            .then(() => {
            return this.createConsumerGroup();
        })
            .then(() => {
            return this.consumeGroup(onEvent);
        });
    }
    createStream() {
        return new Promise((resolve, reject) => {
            try {
                this.stream.getService().getClient().xadd(this.stream.getStreamId(), "*", this.stream.getObjectKey(), "", (error) => {
                    if (!error) {
                        resolve();
                    }
                    else {
                        reject(error);
                    }
                });
            }
            catch (e) {
                reject("Error creating stream: " + e);
            }
        });
    }
    createConsumerGroup() {
        return new Promise((resolve, reject) => {
            try {
                this.stream.getService().getClient().xgroup("CREATE", this.stream.getStreamId(), this.stream.getConsumerGroup(), "$", (error) => {
                    if (!error || error.code === "BUSYGROUP") {
                        resolve();
                    }
                    else {
                        this.logger.error("error" + error);
                        reject(error);
                    }
                });
            }
            catch (e) {
                reject("Error creating consumer group: " + e);
            }
        });
    }
    consumeGroup(onEvent) {
        return new Promise((resolve, reject) => {
            const intervalId = setInterval(() => {
                if (this._ready) {
                    this._ready = false;
                    this.readGroup(reject, onEvent);
                }
            }, RedisStreamConsumer.READ_RATE_IN_MILLISECONDS);
            resolve(new interval_consumer_handler_1.IntervalConsumerHandler(intervalId));
        });
    }
    readGroup(reject, onEvent) {
        try {
            this.stream.getService().getClient().xreadgroup("GROUP", this.stream.getConsumerGroup(), this.stream.getConsumerName(), "NOACK", "STREAMS", this.stream.getStreamId(), ">", (error, rawMessage) => {
                if (!error) {
                    if (rawMessage !== "null" && rawMessage !== null) {
                        onEvent(rawMessage);
                    }
                }
                else {
                    this.logger.error("Error" + error);
                    reject(error);
                }
                this._ready = true;
            });
        }
        catch (e) {
            this.logger.error("Error reading stream: " + e);
        }
    }
}
RedisStreamConsumer.READ_RATE_IN_MILLISECONDS = 10;
exports.RedisStreamConsumer = RedisStreamConsumer;
//# sourceMappingURL=redis-stream.consumer.js.map