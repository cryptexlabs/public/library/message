"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RedisStreamProducer {
    constructor(logger, stream) {
        this.logger = logger;
        this.stream = stream;
    }
    add(message) {
        return this.stream.getService().getClient().xadd(this.stream.getStreamId(), "*", this.stream.getObjectKey(), JSON.stringify(message), (err) => {
            if (err) {
                this.logger.error(err);
            }
        });
    }
}
exports.RedisStreamProducer = RedisStreamProducer;
//# sourceMappingURL=redis-stream.producer.js.map