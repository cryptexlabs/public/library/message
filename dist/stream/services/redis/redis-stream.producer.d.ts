import { RedisStream } from "./redis-stream";
import { StreamProducerInterface } from "../../interfaces/stream-producer.interface";
import { LoggerService } from "@nestjs/common";
export declare class RedisStreamProducer implements StreamProducerInterface {
    private readonly logger;
    private readonly stream;
    constructor(logger: LoggerService, stream: RedisStream);
    add(message: any): Promise<any>;
}
