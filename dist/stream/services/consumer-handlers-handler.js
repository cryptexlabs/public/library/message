"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ConsumerHandlersHandler {
    constructor(handlers) {
        this.handlers = handlers;
    }
    cancel() {
        for (const handler of this.handlers) {
            handler.cancel();
        }
    }
}
exports.ConsumerHandlersHandler = ConsumerHandlersHandler;
//# sourceMappingURL=consumer-handlers-handler.js.map