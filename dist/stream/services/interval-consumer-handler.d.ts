/// <reference types="node" />
import { StreamConsumerHandlerInterface } from "../interfaces";
import Timeout = NodeJS.Timeout;
export declare class IntervalConsumerHandler implements StreamConsumerHandlerInterface {
    private readonly intervalId;
    constructor(intervalId: Timeout);
    cancel(): void;
}
