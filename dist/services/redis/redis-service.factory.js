"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redis_service_1 = require("./redis.service");
class RedisServiceFactory {
    constructor() {
        this.services = [];
    }
    static getInstance() {
        if (!this.instance) {
            this.instance = new RedisServiceFactory();
        }
        return this.instance;
    }
    _getService(logger, host, port, user, password) {
        const endpoint = host + ":" + port;
        if (!this.services[endpoint]) {
            this.services[endpoint] = new redis_service_1.RedisService(logger, {
                host, port, user, password,
            });
        }
        return this.services[endpoint];
    }
    static getRedisService(logger, host, port, user, password) {
        return this.getInstance()._getService(logger, host, port, user, password);
    }
}
exports.RedisServiceFactory = RedisServiceFactory;
//# sourceMappingURL=redis-service.factory.js.map