import { ClientConfigInterface } from "../client-config.interface";
import { LoggerService } from "@nestjs/common";
export declare class RedisService {
    private readonly logger;
    private readonly config;
    private _client;
    private _isDisconnected;
    private _restartCount;
    constructor(logger: LoggerService, config: ClientConfigInterface);
    getClient(): any;
    private _monitorRedis;
    private _initializeRedis;
    private _reInitializeRedis;
}
