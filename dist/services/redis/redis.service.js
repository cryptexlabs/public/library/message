"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const redis = require("redis");
class RedisService {
    constructor(logger, config) {
        this.logger = logger;
        this.config = config;
        this._initializeRedis();
        this._monitorRedis();
    }
    getClient() {
        return this._client;
    }
    _monitorRedis() {
        setInterval(() => {
            if (this._isDisconnected) {
                this._reInitializeRedis();
            }
        }, 1000);
    }
    _initializeRedis() {
        this._isDisconnected = false;
        this._client = redis.createClient(this.config.port, this.config.host);
        this._client.on("connect", () => {
            this.logger.log("Redis: Connected to endpoint: " + this.config.host + ":" + this.config.port);
        });
        this._client.on("error", (err) => {
            this.logger.error("Redis: Error encountered: " + err);
        });
        this._client.on("end", () => {
            this.logger.error("Redis: Connection closed");
            this._isDisconnected = true;
        });
    }
    _reInitializeRedis() {
        if (this._restartCount <= 3) {
            this._initializeRedis();
            this._restartCount++;
        }
        else {
            this.logger.error("Redis: Max retry count reached for " + this.config.host + ":" + this.config.port);
            process.exit(1);
        }
    }
}
exports.RedisService = RedisService;
//# sourceMappingURL=redis.service.js.map