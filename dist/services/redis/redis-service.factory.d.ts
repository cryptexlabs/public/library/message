import { RedisService } from "./redis.service";
import { LoggerService } from "@nestjs/common";
export declare class RedisServiceFactory {
    private static instance;
    private services;
    private constructor();
    private static getInstance;
    private _getService;
    static getRedisService(logger: LoggerService, host: string, port: number, user?: string, password?: string): RedisService;
}
