export interface ClientConfigInterface {
    host: string;
    port: number;
    user?: string;
    password?: string;
}
